## Todo list

This program comes from the simple interest of playing with the Sqlite C interface. Although it accomplish its mission perfectly is quite basic and lacks of many functions that one might expect.
In other words this is interesting as an example, but I doubt it becomes your program of choice.

The build steps are the classic ones, the only thing to have in mind is to install sqlite3.

Once the program is executed a new database is created automatically in <em>./todo.db</em>.

    $ make
    $ ./todo -h
    Usage: ./todo [-b /path/to/database] [-a <value> | -d <index> | -m <index>]

- **-b** indicate the database path to use.
- **-a** creates a new item list.
- **-d** deletes an item by its index.
- **-m** marks an item as done or undone, depending on its previous state.

You can actually pass several options in one call:

    $ ./todo \
       -a 'First item' \
       -a 'This item is deleted later' \
       -m 1 \
       -a 'Adding one more item' \
       -d 2 \
       -m 1 \
       -m 3

    1     27 Aug 2019 22:35                     First item
    3   √ 27 Aug 2019 22:35 27 Aug 2019 22:35   Adding one more item
