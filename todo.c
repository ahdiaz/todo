#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sqlite3.h>


static int
cb_list(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i, t;
    char *row, *id, *subject, *created, *marked, *mark;
    time_t date_t;
    char buff_c[30], buff_m[30];

    for (i = 0; i < argc; i++) {

        if (strcmp(azColName[i], "id") == 0) {

            id = argv[i];

        } else if (strcmp(azColName[i], "subject") == 0) {

            subject = argv[i] ? argv[i] : "";

        } else if (strcmp(azColName[i], "created") == 0) {

            date_t = atoi(argv[i]);
            strftime(buff_c, 30, "%d %b %Y %H:%M", localtime(&date_t));

        } else if (strcmp(azColName[i], "marked") == 0) {

            if (argv[i]) {

                date_t = atoi(argv[i]);
                strftime(buff_m, 30, "%d %b %Y %H:%M", localtime(&date_t));
                mark = "√";

            } else {

                memset(buff_m, 0, 30);
                memset(buff_m, ' ', 17);
                mark = " ";
            }
        }
    }

    t = strlen(id) + strlen(buff_c) + strlen(buff_m) + strlen(subject) + strlen(mark) + 4 + 1;
    row = calloc(t, sizeof(char));

    memcpy(row, id, strlen(id));
    memcpy(row + strlen(row), "\t", 1);
    memcpy(row + strlen(row), mark, strlen(mark));
    memcpy(row + strlen(row), " ", 1);
    memcpy(row + strlen(row), buff_c, strlen(buff_c));
    memcpy(row + strlen(row), "\t", 1);
    memcpy(row + strlen(row), buff_m, strlen(buff_m));
    memcpy(row + strlen(row), "\t", 1);
    memcpy(row + strlen(row), subject, strlen(subject));

    printf("%s\n", row);
    free(row);

    return 0;
}

int
cmd_list(sqlite3 *db)
{
    char *zErrMsg = 0;
    int rc;

    rc = sqlite3_exec(db, "select * from todo order by created desc", cb_list, 0, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return 1;
    }

    return 0;
}

int
cmd_add(sqlite3 *db, char *value)
{
    sqlite3_stmt *stmt;
    int rc;
    time_t now;

    now = time(&now);

    rc = sqlite3_prepare(db, "insert into todo (subject, created) values (?, ?)", -1, &stmt, 0);

    sqlite3_bind_text(stmt, 1, value, strlen(value), 0);
    sqlite3_bind_int(stmt, 2, now);

    sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return 0;
}

int
cmd_del(sqlite3 *db, int index)
{
    sqlite3_stmt *stmt;
    int rc;

    rc = sqlite3_prepare(db, "delete from todo where id = ?", -1, &stmt, 0);

    sqlite3_bind_int(stmt, 1, index);

    sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return 0;
}

static int
cb_mark(void *marked, int argc, char **argv, char **azColName)
{
    time_t *mt = (time_t*) marked;

    *mt = argv[0] == NULL ? 0 : atoi(argv[0]);

    return 0;
}

int
cmd_mark(sqlite3 *db, int index)
{
    sqlite3_stmt *stmt;
    int rc;
    time_t marked = 0;
    char query[100];
    char *zErrMsg = 0;

    sprintf(query, "select marked from todo where id = %d", index);
    rc = sqlite3_exec(db, query, cb_mark, &marked, &zErrMsg);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return 1;
    }

    rc = sqlite3_prepare(db, "update todo set marked = ? where id = ?", -1, &stmt, 0);

    if (marked == 0) {
        sqlite3_bind_int(stmt, 1, time(&marked));
    } else {
        sqlite3_bind_null(stmt, 1);
    }

    sqlite3_bind_int(stmt, 2, index);

    sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return 0;
}

int
todo_init_db(sqlite3 *db)
{
    int rc;
    sqlite3_stmt *stmt;
    char *query = "create table if not exists todo (" \
"id integer primary key autoincrement, " \
"created integer, " \
"marked integer, " \
"subject text" \
")";

    rc = sqlite3_prepare(db, query, -1, &stmt, 0);
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    return 0;
}

char*
get_db_path(char *home_path, char *file_name)
{
    int i;
    char *db_path;

    i = strlen(home_path) + strlen(file_name);
    db_path = calloc(i + 1, sizeof(char));
    memcpy(db_path, home_path, strlen(home_path));
    memcpy(db_path + strlen(home_path), file_name, strlen(file_name));

    return db_path;
}

int
main(int argc, char **argv)
{
    sqlite3 *db;
    int rc, i, index;
    char *db_path, *home_path, *file_name;

    db_path = NULL;

    for (i = 0; i < argc; i++) {

        if (db_path == NULL && strcmp(argv[i], "-b") == 0 && argv[i + 1] != NULL && argv[i + 1][0] != '-') {

            home_path = "";
            file_name = argv[i + 1];

            db_path = get_db_path(home_path, file_name);

        } else if (strcmp(argv[i], "-h") == 0) {

            fprintf(stderr, "Usage: %s [-b <database>] [-a <value> | -d <index> | -m <index>]\n", argv[0]);

            if (db_path != NULL) {
                free(db_path);
            }

            return 1;
        }
    }

    if (db_path == NULL) {

        home_path = getcwd(NULL, 0);
        file_name = "/todo.db";

        db_path = get_db_path(home_path, file_name);
        free(home_path);
    }

    if (db_path == NULL) {
        fprintf(stderr, "Can't open database\n");
        return 1;
    }


    // printf("Reading data from %s\n\n", db_path);

    rc = sqlite3_open(db_path, &db);

    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }

    todo_init_db(db);

    for (i = 0; i < argc; i++) {

        if (strcmp(argv[i], "-a") == 0) {

            if (argv[i + 1] != NULL && argv[i + 1][0] != '-') {
                cmd_add(db, argv[++i]);
            }

        } else if (strcmp(argv[i], "-d") == 0) {

            if (argv[i + 1] != NULL && argv[i + 1][0] != '-') {
                index = atoi(argv[++i]);
                cmd_del(db, index);
            }

        } else if (strcmp(argv[i], "-m") == 0) {

            if (argv[i + 1] != NULL && argv[i + 1][0] != '-') {
                index = atoi(argv[++i]);
                cmd_mark(db, index);
            }
        }
    }

    cmd_list(db);

    sqlite3_close(db);
    if (db_path != NULL) {
        free(db_path);
    }

    return 0;
}
